﻿using MVisa.Integration.Helpers;
using MVisa.Integration.Models;
using MVisa.Integration.Responses;
using MVisa.Integration.Utilities;
using Newtonsoft.Json;
using System;

namespace MVisa.Integration.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Driver driver = new Driver(Constants.PRODUCTION)
            {
                ClientId = "IKIA8750C3364E8D80B3998EA267584348650ECDD382",
                ClientSecret = "waidnbJdsoiISDISNidhIDNiND8382112",
                CurrencyCode = "566",
                MerchantCode = "MX9095",
                MerchantCategoryCode = "6011"
            };
            AccessTokenHelper accessTokenHelper = new AccessTokenHelper();
            QRHelper qrHelper = new QRHelper();
            var tokenResult = accessTokenHelper.GenerateAccessToken(driver, "profile", "client_credentials").Result;
            var deserializedTokenResult = JsonConvert.DeserializeObject<TokenResponse>(tokenResult);
            //var createQRObject = new CreateQR
            //{
            //    amount = 100,
            //    cityName = "Lagos",
            //    country = "NG",
            //    createQRTransaction = "true",
            //    currencyCode = driver.CurrencyCode,
            //    merchantCategoryCode = driver.MerchantCategoryCode,
            //    merchantCode = driver.MerchantCode,
            //    transactionReference = $"WLT{(Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds}",
            //    type = "MVISA"
            //};
            //var createQRResult = qrHelper.Create(deserializedTokenResult.access_token, createQRObject,driver).Result;
            //var deserializedQRResult = JsonConvert.DeserializeObject<CreateQRResponse>(createQRResult);
            //var refreshQRObject = new RefreshQR
            //{
            //    amount = 500,
            //    createQRTransaction = "true",
            //    currencyCode = driver.CurrencyCode,
            //    merchantCode = driver.MerchantCode,
            //    qrCodeId = deserializedQRResult.qrCodeId
            //};
            //var refreshQRResult = qrHelper.Refresh(deserializedTokenResult.access_token, refreshQRObject, driver).Result;
            //var deserializedRefreshQRResult = JsonConvert.DeserializeObject<CreateQRResponse>(refreshQRResult);
            var transactionDetailsObject = new QRTransactionDetails
            {
                merchantCode = driver.MerchantCode,
                transactionReference = "WLT1530855919"
            };
            var transactionDetailsResult = qrHelper.Status(deserializedTokenResult.access_token, transactionDetailsObject, driver).Result;
            var deserializedTransactionDetailsResult = JsonConvert.DeserializeObject<TransactionDetailsResponse>(transactionDetailsResult);
        }
    }
}
