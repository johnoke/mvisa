﻿using MVisa.Integration.Models;
using MVisa.Integration.Utilities;
using System.Threading.Tasks;

namespace MVisa.Integration.Helpers
{
    public class QRHelper
    {
        public async Task<string> Create(string token, CreateQR createQR, Driver driver)
        {
            var response = await new ApiRequest($"{driver.BaseUrl}{GetCreateUrl(driver.Environment)}").MakeRequest(createQR, GeneralUtil.GetAuthorizationHeaders(token), Constants.POST);
            return response;
        }
        public async Task<string> Refresh(string token, RefreshQR refreshQR, Driver driver)
        {
            var response = await new ApiRequest($"{driver.BaseUrl}{GetRefreshUrl(driver.Environment)}").MakeRequest(refreshQR, GeneralUtil.GetAuthorizationHeaders(token), Constants.POST);
            return response;
        }
        public async Task<string> Status(string token, QRTransactionDetails details, Driver driver)
        {
            var url = $"{driver.BaseUrl}{GetStatusUrl(driver.Environment)}?{Constants.TRANSACTIONREFERENCE}={details.transactionReference}&{Constants.MERCHANTCODE}={details.merchantCode}";
            var response = await new ApiRequest(url).GetRequest(GeneralUtil.GetAuthorizationHeaders(token), url);
            return response;
        }
        public string GetCreateUrl(string environment)
        {
            return environment == Constants.STAGING ? Uris.CreateQR : Uris.LiveCreateQR;
        }
        public string GetRefreshUrl(string environment)
        {
            return environment == Constants.STAGING ? Uris.RefreshQR : Uris.LiveRefreshQR;
        }
        public string GetStatusUrl(string environment)
        {
            return environment == Constants.STAGING ? Uris.TransactionDetails : Uris.LiveTransactionDetails;
        }
    }
}
