﻿using MVisa.Integration.Utilities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MVisa.Integration.Helpers
{
    public class AccessTokenHelper
    {
        public async Task<string> GenerateAccessToken(Driver driver, string scope, string grantType)
        {
            byte[] bytes;
            bytes = Encoding.UTF8.GetBytes($"{driver.ClientId}:{driver.ClientSecret}");
            var authorization = $"{Constants.BASIC} {Convert.ToBase64String(bytes)}";
            var headers = new List<KeyValuePair<string, string>>();
            headers.Add(new KeyValuePair<string, string>(Constants.AUTHORIZATION, authorization));
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>(Constants.GRANTTYPE, grantType));
            keyValues.Add(new KeyValuePair<string, string>(Constants.SCOPE, scope));
            var response = await new ApiRequest(null).MakeURLEncodedRequest(HttpMethod.Post, driver.TokenBaseUrl, Uris.TokenEndPoint, keyValues, headers);
            return response;
        }
    }
}
