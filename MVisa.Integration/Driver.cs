﻿using MVisa.Integration.Utilities;

namespace MVisa.Integration
{
    public class Driver
    {
        public Driver(string environment)
        {
            BaseUrl = environment == Constants.STAGING ? Uris.StagingUrl : Uris.LiveUrl;
            TokenBaseUrl = environment == Constants.STAGING ? Uris.StagingTokenUrl : Uris.LiveTokenUrl;
        }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Token { get; set; }
        public string MerchantCode { get; set; }
        public string CurrencyCode { get; set; }
        public string MerchantCategoryCode { get; set; }
        public string BaseUrl { get; set; }
        public string TokenBaseUrl { get; set; }
        public string Environment { get; set; }
    }
}
