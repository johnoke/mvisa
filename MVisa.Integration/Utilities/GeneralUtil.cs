﻿using System;
using System.Collections.Generic;

namespace MVisa.Integration.Utilities
{
    public class GeneralUtil
    { 
        public static List<KeyValuePair<string, string>> GetAuthorizationHeaders(string token)
        {
            var timeStamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var nonce = Guid.NewGuid().ToString();
            var headers = new List<KeyValuePair<string, string>>();
            headers.Add(new KeyValuePair<string, string>(Constants.AUTHORIZATION, $"{Constants.BEARER} {token}"));
            headers.Add(new KeyValuePair<string, string>(Constants.NONCE, nonce));
            headers.Add(new KeyValuePair<string, string>(Constants.TIMESTAMP, timeStamp.ToString()));
            return headers;
        }
    }
}
