﻿namespace MVisa.Integration.Utilities
{
    public class Uris
    {
        public const string StagingUrl = "https://qa.interswitchng.com/";
        public const string LiveUrl = "https://www.interswitchgroup.com/";
        public const string TokenEndPoint = "/passport/oauth/token";
        public const string CreateQR = "till/paymentgateway/api/v2/payable/qrcodes";
        public const string RefreshQR = "till/paymentgateway/api/v1/payable/qrcodes/refresh";
        public const string TransactionDetails = "till/paymentgateway/api/v2/transactions/qr";
        public const string LiveCreateQR = "paymentgateway/api/v2/payable/qrcodes";
        public const string LiveRefreshQR = "paymentgateway/api/v1/payable/qrcodes/refresh";
        public const string LiveTransactionDetails = "paymentgateway/api/v2/transactions/qr";
        public const string StagingTokenUrl = "https://apps.qa.interswitchng.com";
        public const string LiveTokenUrl = "https://passport.interswitchng.com";
    }
}
