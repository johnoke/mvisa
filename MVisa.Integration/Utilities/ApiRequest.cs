﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Text;
using System.IO;

namespace MVisa.Integration.Utilities
{
    public class ApiRequest
    {
        public object Data { get; set; }
        public string Url { get; set; }
        public ApiRequest(string url)
        {
            this.Url = url;
        }
        public async Task<string> MakeURLEncodedRequest(HttpMethod method, string baseUrl, string endPoint, object data = null, List<KeyValuePair<string, string>> headers = null)
        {
            var client = new HttpClient();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            client.Timeout = TimeSpan.FromMinutes(5);
            client.BaseAddress = new Uri(baseUrl);
           
            var request = new HttpRequestMessage(method, endPoint);
            if (data != null)
            {
                request.Content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)data);
            }
            var responseData = await client.SendAsync(request);
            var response = await responseData.Content.ReadAsStringAsync();

            return response;
        }
        public async Task<string> MakeRequest(object data = null, List<KeyValuePair<string, string>> headers = null, string method = Constants.GET)
        {
            var client = new HttpClient();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //client.BaseAddress = new Uri(this.Url);
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            HttpResponseMessage response = null;
            client.Timeout = TimeSpan.FromMinutes(5);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            if (method.Equals(Constants.POST))
            {
                if (data != null)
                {
                    response = await client.PostAsJsonAsync(this.Url, data);
                }
                else
                {
                    response = await client.PostAsJsonAsync(this.Url, new { });
                }
            }
            else if (method.Equals(Constants.GET))
            {
                response = await client.GetAsync(this.Url);
            }
            var httpStatusCode = (int)response.StatusCode;
            if (httpStatusCode == 200 || httpStatusCode == 201 || httpStatusCode == 202)
            {
                return await response.Content.ReadAsStringAsync();
            }
            return await response.Content.ReadAsStringAsync();
        }
        public async Task<string> GetRequest(List<KeyValuePair<string, string>> headers, string url)
        {
            string MoodysWebstring = @url;
            Uri MoodysWebAddress = new Uri(MoodysWebstring);
            HttpWebRequest request = WebRequest.Create(MoodysWebAddress) as HttpWebRequest;
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }
            request.ContentType = "application/json";
            request.Method = "GET";
            string results = string.Empty;
            HttpWebResponse response;
            using (response = await request.GetResponseAsync() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                results = reader.ReadToEnd();
            }
            return results;
        }
    }
}
