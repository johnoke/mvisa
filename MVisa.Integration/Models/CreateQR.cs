﻿namespace MVisa.Integration.Models
{
    public class CreateQR
    {
        public string merchantCode { get; set; }
        public string type { get; set; }
        public decimal amount { get; set; }
        public string currencyCode { get; set; }
        public string merchantCategoryCode { get; set; }
        public string country { get; set; }
        public string cityName { get; set; }
        public string createQRTransaction { get; set; }
        public string transactionReference { get; set; }
    }
}
