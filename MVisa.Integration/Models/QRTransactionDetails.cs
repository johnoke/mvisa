﻿namespace MVisa.Integration.Models
{
    public class QRTransactionDetails
    {
        public string transactionReference { get; set; }
        public string merchantCode { get; set; }
    }
}
