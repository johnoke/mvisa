﻿namespace MVisa.Integration.Models
{
    public class RefreshQR
    {
        public string merchantCode { get; set; }
        public decimal amount { get; set; }
        public string currencyCode { get; set; }
        public string createQRTransaction { get; set; }
        public string transactionReference { get; set; }
        public string qrCodeId { get; set; }
    }
}
