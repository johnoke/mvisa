﻿namespace MVisa.Integration.Responses
{
    public class CreateQRResponse
    {
        public string transactionReference { get; set; }
        public string qrCodeData { get; set; }
        public string qrCodeIdMVisa { get; set; }
        public string qrCodeId { get; set; }
    }
}
