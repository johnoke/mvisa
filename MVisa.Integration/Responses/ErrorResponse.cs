﻿namespace MVisa.Integration.Responses
{
    public class ErrorResponse
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}
