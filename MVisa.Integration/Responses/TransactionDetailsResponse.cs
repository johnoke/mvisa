﻿namespace MVisa.Integration.Responses
{
    public class TransactionDetailsResponse
    {
        public decimal amount { get; set; }
        public decimal surcharge { get; set; }
        public string transactionReference { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public string merchantCode { get; set; }
        public string merchantCustomerName { get; set; }
        public string channel { get; set; }
        public string channelTransactionReference { get; set; }
        public string settlementStatus { get; set; }
        public string remittanceStatus { get; set; }
        public string dateOfPayment { get; set; }
        public decimal remittanceAmount { get; set; }
        public string acquirerCode { get; set; }
        public string currencyCode { get; set; }
    }
}
